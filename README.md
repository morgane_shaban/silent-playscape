# Silent Playscape

Silent Playscape is an upscaled version of the Silent Game originally designed by John Habraken and Mark D Gross in 1987. This is the documentation, description and reflection of Morgane's process, developing the playscape.

## What is the Silent Playscape?
The Silent Playscape is a undirected play tool, where players sculpt, build and make their own play in a collaborative silence. It is a co-creation game that uses implicit communication. It is an up scaled version of the Silent Board Game by Ines Budriles and Morgane Shaban (please find this repo here: https://gitlab.com/morgane_shaban/silent-board-game.git). They are both inspired by the Silent Game which was originally designed by John Habraken and Mark D gross and can be found in the 1987 book; Concept Design Games. The game has become spacial and active. The Silent Playscape consists of different pieces made from different biodegradable materials including bioplastics, wood and canvas, to foster a critical view of the material world around us. This free play tool allows you to use your entire body to move, place and be a part of the final sculpture.

## Plan and Execution of the Project
The process of making the pieces of the game have been centred around the process of making the bioplastic pieces. All the materials needed to make the biomaterial will create more pieces (see diagram below). For example the CNC plywood stencils used to pour the biomaterial will also be a part of the game. Or the fabric backing the stencil mould will be used to make cushioned pieces (stuffed with upcycled disposable face masks). The is full process will be tested in the Fabacademy microchallenge 04 to understand each step of making the different materialed pieces.

![layered process diagram](images/diagram.jpg)

*the layered diagram of fabrication process of pieces*

## Iteration Process
This project builds on top of the Silent Board Game designed by myslef (Morgane Shaban) and Ines Burdiles. This iterations, the prototype of the Silent Playscape’s fabrication process lasted around 3-4 days where I faced several successes and challenges. The CNC process being simple was straight forward and with little issue. The only one being some extra sanding of the notches in the shapes for them to slot more smoothly for the children. The biomaterial making process is never as easy to control and often does not go as plan. This being the first time making large scaled bioplastics meant that this was an experiment. For this reason I decided to use a gelatine based biomaterial as I knew if things did not go to plan I could melt down the material again and recast it (which turned out to be a good idea). The main issue I had with the biomaterial process was the issue with the moulds leaking. This is in the process of being further explored, potentially using a glue gun next to seal the edges. The recipes also have gone through an iterative process and adapted throughout the week. For example the wax was not a successful addition as it did not melt and dissolve into the solution but rather sat on top of the mixture, there for the next recipe did not have it added. Finally I was not able to make the fabric cushioned pieces as I ran out of time, but I have managed to prepare the collected single use face masks for the stuffing of the cushions. The files to laser cut the material are also ready for next week. Unfortunately due to biomaterials having to be poured onto an non porous fabric the left over canvas I had could not be used for the mould. However I have decided I will still use it for the cushioned pieces.

![digital model](images/digital model.jpg)

*digital model of plywood pieces for CNC*

## What materials do you need?

Plywood Pieces
- 9mm plywood 2440 x 1220cm

Bioplastic Pieces
- waterproof fabric
- duck tape
- 11.5L water (avocado pit dyed water)
- 2000gr gelatine
- 2000gr glycerine
- 40g alumbre
- 10g cinnamon/ clove/ rosemary essential oil
- 30-45g turmeric powder
- 500g Carnauba wax*
*due to wax not not being successful I would took this out of the recipe

Canvas Pieces
- cotton Canvas
- used disposable facemasks

![taping moulds](images/ingredients.jpg)

*biomaterial ingredients*

## Step by Step _
## F A B R I C A T I O N  O F  P L Y W O O D

- Measure the average width of your board with a caliper. (e.g the 9mm plywood board used was 10.27mm average)
- secure the material to the machine (using the CNC drill, pilot wholes for locations of where you will drill the board onto the sacrificial board)
- Do the kerf test for slots (for example vary tests by adding 0.10mm, 0.25mm, 0.35mm)*

- CNC mill the pieces:
  - End mill: 6mm, 1 flute, flat.
  - Cut direction : climb (down cut)
  - Speed : 18.000 RPM (search in manufacturer of the end mill)
  - Plunge: 3500
  - Cut : 4500
  - Retract : 4500
  - Clearance Plane : 20 on Z
- remove pieces and sand them down to get desired finish

*Tip: Remember to do a kerf test or you might have to do a lot of sanding. You want the slot to be easy to put together and remove for children

![CNC pieces](images/CNC.GIF)

*CNCing*

![CNC pieces](images/CNCPieces.jpg)

*CNC Pieces post cut*

## Step by Step _
## F A B R I C A T I O N  O F  B I O M A T E R I A L

prep your moulds by securing moulds to waterproof fabric (ducktape/ hot glue gun)


![taping moulds](images/taping.jpg)

*preparing moulds*

Recipe 01 - for one 1140 x 1140 x 5mm square mould, 6.5L volume
i n g r e d i e n t s
- 6.5L avocado pit dyed water
- 1000gr gelatine
- 1000gr glycerine
- 20g alumbre
- 10g cinnamon/ clove/ rosemary essential oil
- 500g carnauba wax

On high heat (100°C), bring the water to a boil. Start adding the gelatine bit by bit whilst continuously stirring making sure there are no lumps. Once all the gelatine is well mixed stir for approximate 10 minutes more. Reduce heat to 80°C then add all the glycerine and mix well. Continue stirring for another 10 minutes. Add the alumbre and keep stirring. Melt the carnauba wax in a separate pot at 120°C separate. (always stirring the gelatine mixture). Add the essential oil to the gelatine mixture and stir. Do the stick test to check if the mixture is ready (get a bit of the mixture on your finger and thumb if it is tacky/sticky then the mix is ready). Once it is ready add the melted wax and stir well. Scoop out any bubbles. Pour quickly into the moulds (make sure the mould is on even ground and not leaking as you pour). Using a tissue pop the bubbles on the surface. Leave the material to set in a warm, dry well ventilated room for approx 5 hours. Then prop up the material in the stencil vertically and leave for another 2 days before releasing it from the mould.

![stirring](images/stirring.GIF)

*cooking materials*

Recipe 02 - for six abstract shaped moulds, 5L volume
i n g r e d i e n t s
- 5L water
- 1000gr gelatine
- 1000gr glycerine
- 30-45g turmeric powder
- 20g alumbre
- 10g cinnamon/ clove/ rosemary essential oil

On high heat (100°C), bring the water to a boil. Start adding the gelatine bit by bit whilst continuously stirring making sure there are no lumps. Once all the gelatine is well mixed stir for approximate 10 minutes more. Reduce heat to 80°C then add all the glycerine and mix well. Continue stirring for another 10 minutes. Add the alumbre and keep stirring. Add the turmeric powder and mix for another 10 minutes. Add the essential oil to the gelatine mixture and stir. Do the stick test to check if the mixture is ready (get a bit of the mixture on your finger and thumb if it is tacky/sticky then the mix is ready). Once it is ready add the melted wax and stir well. Scoop out any bubbles. Pour quickly into the moulds (make sure the mould is on even ground and not leaking as you pour). Using a tissue pop the bubbles on the surface. Leave the material to set in a warm, dry well ventilated room for approx 5 hours. Then prop up the material in the stencil vertically and leave for another 2 days before releasing it from the mould.

![leaking](images/leaking.jpg)

*materials leaking*

![leaking](images/leaking2.jpg)

*cleaning up*

Tip: Tape was not a sufficient to stop leaking, maybe use a hot glue gun
Tip: do not use carnauba wax (it does not dissolve well in the recipe but instead sits on top)


## Future Developments
- to complete the game and make the other pieces (CNC more pieces, make cushioned pieces and remelt and recast biomaterials that were not successful)
- a future project could be to make the game with the children/ players


## What you will find in this repo
3D Rhino model files
CNC files of plywood pieces
Biomaterial recipes 
Kerf test file 
Images of the process
Tips and learnings from the process


## Links to Related Posts
- Morgane Sha'ban personal website post : https://morgane_shaban.gitlab.io/mdef/challenge03.html
- Ines Budriles and Morgane Shaban Silent Board Game repository https://gitlab.com/morgane_shaban/silent-board-game.git)
